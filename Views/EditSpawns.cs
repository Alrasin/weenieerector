﻿using System;
using VirindiViewService;
using VirindiViewService.Controls;
using WeenieErector.Data;
using WeenieErector.Data.WorldSpawns;
using WeenieErector.Lib;

namespace WeenieErector.Views {
    public class EditSpawns : IDisposable {
        HudList UIEditSpawnsList { get; set; }
        HudButton UIEditSpawnTeleportHere { get; set; }
        HudStaticText UICurrentSpawnFile { get; set; }

        HudStaticText UIEditSpawnID { get; set; }
        HudCombo UIEditSpawnLinkWithParent { get; set; }

        HudTextBox UIEditSpawnOriginX { get; set; }
        HudTextBox UIEditSpawnOriginY { get; set; }
        HudTextBox UIEditSpawnOriginZ { get; set; }
        HudTextBox UIEditSpawnAngleW { get; set; }
        HudTextBox UIEditSpawnAngleX { get; set; }
        HudTextBox UIEditSpawnAngleY { get; set; }
        HudTextBox UIEditSpawnAngleZ { get; set; }
        HudTextBox UIEditSpawnDescription { get; set; }
        HudTextBox UIEditSpawnWcid { get; set; }
        HudTextBox UIEditSpawnObjectCellID { get; set; }

        HudButton UIEditSpawnSetToMyOrigin { get; set; }
        HudButton UIEditSpawnSetToMyAngle { get; set; }
        HudButton UIEditSpawnUpdateSpawn { get; set; }

        HudStaticText UICurrentLandBlock { get; set; }

        Weenie currentlyEditing = null;
        uint currentLandblock = 0;

        private bool disposed = false;

        public EditSpawns(HudView view) {
            UIEditSpawnsList = (HudList)view["EditSpawnsList"];
            UIEditSpawnTeleportHere = (HudButton)view["EditSpawnTeleportHere"];
            UICurrentSpawnFile = (HudStaticText)view["CurrentSpawnFile"];

            UIEditSpawnID = (HudStaticText)view["EditSpawnID"];
            UIEditSpawnLinkWithParent = (HudCombo)view["EditSpawnLinkWithParent"];

            UIEditSpawnOriginX = (HudTextBox)view["EditSpawnOriginX"];
            UIEditSpawnOriginY = (HudTextBox)view["EditSpawnOriginY"];
            UIEditSpawnOriginZ = (HudTextBox)view["EditSpawnOriginZ"];

            UIEditSpawnAngleW = (HudTextBox)view["EditSpawnAngleW"];
            UIEditSpawnAngleX = (HudTextBox)view["EditSpawnAngleX"];
            UIEditSpawnAngleY = (HudTextBox)view["EditSpawnAngleY"];
            UIEditSpawnAngleZ = (HudTextBox)view["EditSpawnAngleZ"];

            UIEditSpawnDescription = (HudTextBox)view["EditSpawnDescription"];
            UIEditSpawnWcid = (HudTextBox)view["EditSpawnWcid"];
            UIEditSpawnObjectCellID = (HudTextBox)view["EditSpawnObjectCellID"];

            UIEditSpawnSetToMyOrigin = (HudButton)view["EditSpawnSetToMyOrigin"];
            UIEditSpawnSetToMyAngle = (HudButton)view["EditSpawnSetToMyAngle"];
            UIEditSpawnUpdateSpawn = (HudButton)view["EditSpawnUpdateSpawn"];

            UICurrentLandBlock = (HudStaticText)view["CurrentLandBlock"];

            UIEditSpawnsList.Click += UIEditSpawnsList_Click;
            UIEditSpawnTeleportHere.Hit += UIEditSpawnTeleportHere_Hit;
            UIEditSpawnSetToMyOrigin.Hit += UIEditSpawnSetToMyOrigin_Hit;
            UIEditSpawnSetToMyAngle.Hit += UIEditSpawnSetToMyAngle_Hit;
            UIEditSpawnUpdateSpawn.Hit += UIEditSpawnUpdateSpawn_Hit;

            Globals.LocationManager.LandblockChanged += LocationManager_LandblockChanged;
            Globals.SpawnManager.SpawnCreated += SpawnManager_SpawnCreated;
            Globals.WeenieSource.WeeniesLoaded += WeenieSource_WeeniesLoaded;
            Globals.SpawnManager.SpawnFileImported += SpawnManager_SpawnFileImported;
            Globals.SpawnManager.SpawnsChanged += SpawnManager_SpawnsChanged;
            Globals.SpawnManager.SpawnDeleted += SpawnManager_SpawnDeleted;

            RefreshList();
        }

        private void SpawnManager_SpawnDeleted(object sender, SpawnDeletedEventArgs e) {
            try {
                if (currentlyEditing != null && currentlyEditing.id == e.Deleted.id) {
                    currentlyEditing = null;
                    ClearEditFields();
                }
            }
            catch (Exception ex) { Util.LogException(ex); }
        }

        private void SpawnManager_SpawnsChanged(object sender, SpawnsChangedEventArgs e) {
            try {
                var scroll = UIEditSpawnsList.ScrollPosition;
                
                RefreshList();

                UIEditSpawnsList.ScrollPosition = Math.Min(scroll, UIEditSpawnsList.MaxScroll);
            }
            catch (Exception ex) { Util.LogException(ex); }
        }

        private void SpawnManager_SpawnFileImported(object sender, SpawnFileImportedEventArgs e) {
            try {
                RefreshList();
                ClearEditFields();
            }
            catch (Exception ex) { Util.LogException(ex); }
        }

        private void WeenieSource_WeeniesLoaded(object sender, WeeniesLoadedEventArgs e) {
            try {
                RefreshList();
                ClearEditFields();
            }
            catch (Exception ex) { Util.LogException(ex); }
        }

        private void UIEditSpawnUpdateSpawn_Hit(object sender, EventArgs e) {
            try {
                double ox = 0;
                double oy = 0;
                double oz = 0;
                double aw = 0;
                double ax = 0;
                double ay = 0;
                double az = 0;
                int wcid = 0;
                uint objcell_id = 0;

                if (!Int32.TryParse(UIEditSpawnWcid.Text, out wcid)) {
                    Util.WriteToChat("Unable to parse wcid to int32: " + UIEditSpawnWcid.Text);
                    return;
                }
                if (!UInt32.TryParse(UIEditSpawnObjectCellID.Text, out objcell_id)) {
                    Util.WriteToChat("Unable to parse objcell_id to uint32: " + UIEditSpawnObjectCellID.Text);
                    return;
                }

                if (!Double.TryParse(UIEditSpawnOriginX.Text, out ox)) {
                    Util.WriteToChat("Unable to parse origin X to double: " + UIEditSpawnOriginX.Text);
                    return;
                }
                if (!Double.TryParse(UIEditSpawnOriginY.Text, out oy)) {
                    Util.WriteToChat("Unable to parse origin Y to double: " + UIEditSpawnOriginY.Text);
                    return;
                }
                if (!Double.TryParse(UIEditSpawnOriginZ.Text, out oz)) {
                    Util.WriteToChat("Unable to parse origin Z to double: " + UIEditSpawnOriginZ.Text);
                    return;
                }
                if (!Double.TryParse(UIEditSpawnAngleW.Text, out aw)) {
                    Util.WriteToChat("Unable to parse angle W to double: " + UIEditSpawnAngleW.Text);
                    return;
                }
                if (!Double.TryParse(UIEditSpawnAngleX.Text, out ax)) {
                    Util.WriteToChat("Unable to parse angle X to double: " + UIEditSpawnAngleX.Text);
                    return;
                }
                if (!Double.TryParse(UIEditSpawnAngleY.Text, out ay)) {
                    Util.WriteToChat("Unable to parse angle Y to double: " + UIEditSpawnAngleY.Text);
                    return;
                }
                if (!Double.TryParse(UIEditSpawnAngleZ.Text, out az)) {
                    Util.WriteToChat("Unable to parse angle Z to double: " + UIEditSpawnAngleZ.Text);
                    return;
                }

                var pos = new WeeniePos();
                pos.objcell_id = objcell_id;
                pos.frame.origin.x = ox;
                pos.frame.origin.y = oy;
                pos.frame.origin.z = oz;
                pos.frame.angles.w = aw;
                pos.frame.angles.x = ax;
                pos.frame.angles.y = ay;
                pos.frame.angles.z = az;

                int parentId = 0;

                if (UIEditSpawnLinkWithParent.Current > 0) {
                    var text = ((HudStaticText)UIEditSpawnLinkWithParent[UIEditSpawnLinkWithParent.Current]).Text;
                    Int32.TryParse(text.Split(' ')[0], out parentId);
                }

                Globals.SpawnManager.EditWeenie(currentlyEditing, wcid, UIEditSpawnDescription.Text, pos, parentId);
            }
            catch (Exception ex) { Util.LogException(ex); }
        }

        private void SpawnManager_SpawnCreated(object sender, SpawnCreatedEventArgs e) {
            try {
                if (e.Created == null) return;

                var row = UIEditSpawnsList.AddRow();

                ((HudStaticText)row[0]).Text = e.Created.id.ToString();
                ((HudStaticText)row[1]).Text = e.Created.wcid.ToString();
                ((HudStaticText)row[2]).Text = e.Created.desc.ToString();
                ((HudPictureBox)row[3]).Image = 4600 + 0x6000000; // remove icon

                // scroll to bottom
                UIEditSpawnsList.ScrollPosition = UIEditSpawnsList.MaxScroll;

                // select newly added
                UIEditSpawnsList_Click(null, UIEditSpawnsList.RowCount - 1, 0);
            }
            catch (Exception ex) { Util.LogException(ex); }
        }

        private void LocationManager_LandblockChanged(object sender, LandblockChangedEventArgs e) {
            try {
                UICurrentLandBlock.Text = e.Landblock.ToString();
                currentLandblock = e.Landblock;
                RefreshList();
            }
            catch (Exception ex) { Util.LogException(ex); }
        }

        private void UIEditSpawnSetToMyAngle_Hit(object sender, EventArgs e) {
            if (currentlyEditing == null) {
                Util.WriteToChat("Not currently editing a spawn, cant set angles");
                return;
            }

            var orientation = Globals.Core.WorldFilter[Globals.Core.CharacterFilter.Id].Orientation();

            UIEditSpawnAngleW.Text = orientation.W.ToString();
            UIEditSpawnAngleX.Text = orientation.X.ToString();
            UIEditSpawnAngleY.Text = orientation.Y.ToString();
            UIEditSpawnAngleZ.Text = orientation.Z.ToString();
        }

        private void UIEditSpawnSetToMyOrigin_Hit(object sender, EventArgs e) {
            if (currentlyEditing == null) {
                Util.WriteToChat("Not currently editing a spawn, cant set origin");
                return;
            }

            UIEditSpawnOriginX.Text = Globals.Core.Actions.LocationX.ToString();
            UIEditSpawnOriginY.Text = Globals.Core.Actions.LocationY.ToString();
            UIEditSpawnOriginZ.Text = Globals.Core.Actions.LocationZ.ToString();
            UIEditSpawnObjectCellID.Text = (Util.CurrentLandblock() + Util.CurrentCell()).ToString();
        }

        private void UIEditSpawnTeleportHere_Hit(object sender, EventArgs e) {
            if (currentlyEditing == null) {
                Util.WriteToChat("Not currently editing a spawn, cant teleport.");
                return;
            }

            currentlyEditing.TeleportTo();
        }

        private void UIEditSpawnsList_Click(object sender, int row, int col) {
            var weenies = Globals.SpawnManager.GetWeeniesFromLandblock(currentLandblock);
            var weenie = weenies[row];

            RedrawParentList();

            // delete
            if (col == 3) {
                Globals.SpawnManager.DeleteWeenie(weenie);
                return;
            }

            EditWeenieSpawn(weenie);

            var landblock = Util.CurrentLandblock();
            int lbxoffset = (int)(Math.Floor((double)(landblock) / (double)(0x1000000))) & 0xFF;
            int lbyoffset = (int)(Math.Floor((double)(landblock) / (double)(0x10000))) & 0xFF;
            var x = ((double)(lbxoffset - 0x7F) * 192 + weenie.pos.frame.origin.x - 84) / 240d;
            var y = ((double)(lbyoffset - 0x7F) * 192 + weenie.pos.frame.origin.y - 84) / 240d;

            Util.WriteToChat(weenie.ToString().Replace("\r", ""));
            Util.WriteToChat(string.Format("World Coordinates: {0}", Util.FormatCoordinates(x, y)));

            var woid = Globals.SpawnManager.FindWeenieWorldObjectId(weenie);
            Util.WriteToChat("woid: " + woid.ToString());
            if (woid != 0) {
                Globals.Core.Actions.SelectItem(woid);
            }
        }

        private void RedrawParentList() {
            var selected = UIEditSpawnLinkWithParent.Count > 0 ? ((HudStaticText)UIEditSpawnLinkWithParent[UIEditSpawnLinkWithParent.Current]).Text : "0";
            var newSelected = 0;
            var weenies = Globals.SpawnManager.GetWeeniesFromLandblock(Util.CurrentLandblock());
            var index = 1;

            UIEditSpawnLinkWithParent.Clear();

            UIEditSpawnLinkWithParent.AddItem("None", "None");

            foreach (var weenie in weenies) {
                var name = string.Format("{0} - {1}", weenie.id, weenie.desc);
                UIEditSpawnLinkWithParent.AddItem(name, name);
                if (name == selected) {
                    newSelected = index;
                }
                index++;
            }
            UIEditSpawnLinkWithParent.Current = newSelected;

            Util.WriteToChat("Redraw parents list");
        }

        private void EditWeenieSpawn(Weenie weenie) {
            var weenieName = Globals.WeenieSource.Weenies.ContainsKey(weenie.wcid) ? Globals.WeenieSource.Weenies[weenie.wcid] : "Unknown";

            var weenieParent = Globals.SpawnManager.GetWeenieParent(weenie);

            currentlyEditing = weenie;

            if (weenieParent != null) {
                var match = string.Format("{0} - {1}", weenieParent.id, weenieParent.desc);
                for (var i = 0; i < UIEditSpawnLinkWithParent.Count; i++) {
                    var row = ((HudStaticText)UIEditSpawnLinkWithParent[i]);

                    if (row.Text == match) {
                        UIEditSpawnLinkWithParent.Current = i;
                        break;
                    }
                }
            }
            else {
                UIEditSpawnLinkWithParent.Current = 0;
            }

            //UIEditSpawnHeader.Text = string.Format("{0} - {1}", weenie.wcid, weenieName);

            UIEditSpawnOriginX.Text = weenie.pos.frame.origin.x.ToString();
            UIEditSpawnOriginY.Text = weenie.pos.frame.origin.y.ToString();
            UIEditSpawnOriginZ.Text = weenie.pos.frame.origin.z.ToString();

            UIEditSpawnAngleW.Text = weenie.pos.frame.angles.w.ToString();
            UIEditSpawnAngleX.Text = weenie.pos.frame.angles.x.ToString();
            UIEditSpawnAngleY.Text = weenie.pos.frame.angles.y.ToString();
            UIEditSpawnAngleZ.Text = weenie.pos.frame.angles.z.ToString();

            UIEditSpawnDescription.Text = weenie.desc;
            UIEditSpawnObjectCellID.Text = weenie.pos.objcell_id.ToString();
            UIEditSpawnWcid.Text = weenie.wcid.ToString();
            UIEditSpawnID.Text = weenie.id.ToString();
        }

        public void ClearEditFields() {
            currentlyEditing = null;

            //UIEditSpawnHeader.Text = "";
            UIEditSpawnOriginX.Text = "";
            UIEditSpawnOriginY.Text = "";
            UIEditSpawnOriginZ.Text = "";
            UIEditSpawnAngleW.Text = "";
            UIEditSpawnAngleX.Text = "";
            UIEditSpawnAngleY.Text = "";
            UIEditSpawnAngleZ.Text = "";
            UIEditSpawnDescription.Text = "";
            UIEditSpawnID.Text = "";
            UIEditSpawnObjectCellID.Text = "";
            UIEditSpawnWcid.Text = "";
        }

        public bool IsEditing() {
            return currentlyEditing != null;
        }

        public void RefreshList() {
            UIEditSpawnsList.ClearRows();

            foreach (var weenie in Globals.SpawnManager.GetWeeniesFromLandblock(Util.CurrentLandblock())) {
                var row = UIEditSpawnsList.AddRow();

                ((HudStaticText)row[0]).Text = weenie.id.ToString();
                ((HudStaticText)row[1]).Text = weenie.wcid.ToString();
                ((HudStaticText)row[2]).Text = weenie.desc.ToString();
                ((HudPictureBox)row[3]).Image = 4600 + 0x6000000; // remove icon
            }
        }

        public void SetLoadedFile(string name) {
            UICurrentSpawnFile.Text = name;
        }

        public void Dispose() {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing) {
            if (!disposed) {
                if (disposing) {
                    Globals.LocationManager.LandblockChanged -= LocationManager_LandblockChanged;
                    Globals.SpawnManager.SpawnCreated -= SpawnManager_SpawnCreated;
                }
                disposed = true;
            }
        }
    }
}
