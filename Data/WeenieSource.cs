﻿using Microsoft.VisualBasic.FileIO;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using WeenieErector.Lib;

namespace WeenieErector.Data {
    public class WeeniesLoadedEventArgs : EventArgs {
    }

    public class WeenieSource {
        public bool IsDownloading = false;
        public Dictionary<int, string> Weenies = new Dictionary<int, string>();
        private string csvData = "";

        private string WeeniesURL = "https://docs.google.com/spreadsheets/d/18GXhnoO33eTEst5z17daaeSSp9fyFEt2HijZhovnpIY/export?format=csv&id=18GXhnoO33eTEst5z17daaeSSp9fyFEt2HijZhovnpIY&gid=1416405999";
        
        public event EventHandler<WeeniesLoadedEventArgs> WeeniesLoaded;

        public void LoadCache() {
            if (File.Exists(CSVCachePath())) {
                ParseWeeniesCSV(File.ReadAllText(CSVCachePath()));
            }
        }

        private string CSVCachePath() {
            return Path.Combine(Util.GetPluginStoragePath(), "weenies.csv");
        }

        protected virtual void OnWeeniesLoaded(WeeniesLoadedEventArgs e) {
            WeeniesLoaded?.Invoke(this, e);
        }

        public void Download() {
            IsDownloading = true;
            Weenies.Clear();

            var request = new Newtonsoft.Json.Serialization.Action(FetchCSV).BeginInvoke(new AsyncCallback(FetchCSVComplete), null);
        }

        public void FetchCSV() {
            try {
                // content creation wcids tab, expects csv of weenies
                // only the two first columns are required to be present
                /*
                 *  weenieClassId,weenieClassDescription,New,Comments
                 *  1,Player Template,,
                 *  2,,,
                 *  3,Olthoi Worker,,
                 */
                csvData = "";

                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(WeeniesURL);
                request.Timeout = 15000;
                request.AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate;

                using (HttpWebResponse response = (HttpWebResponse)request.GetResponse()) {
                    using (Stream stream = response.GetResponseStream()) {
                        using (StreamReader reader = new StreamReader(stream)) {
                            csvData = reader.ReadToEnd();
                        }
                    }
                }
            }
            catch (Exception ex) {
                Util.WriteToChat("Unable to fetch weenies.csv from: " + WeeniesURL);
                Util.LogException(ex);
            }
        }

        private void FetchCSVComplete(IAsyncResult ar) {
            try {
                IsDownloading = false;

                ParseWeeniesCSV(csvData);

                Util.WriteToChat("Save cache: " + Weenies.Count.ToString());
                if (Weenies.Count > 0) {
                    // save cached copy
                    File.WriteAllText(CSVCachePath(), csvData);
                }
            }
            catch (Exception ex) { Util.LogException(ex); }
        }

        public void ParseWeeniesCSV(string csv) {
            Weenies.Clear();

            using (TextFieldParser parser = new TextFieldParser(new StringReader(csv))) {
                parser.CommentTokens = new string[] { "#" };
                parser.SetDelimiters(new string[] { "," });
                parser.HasFieldsEnclosedInQuotes = true;

                // Skip over header line.
                parser.ReadLine();

                while (!parser.EndOfData) {
                    try {
                        var values = new List<string>();

                        var readFields = parser.ReadFields();
                        if (readFields != null) {
                            values.AddRange(readFields);

                            int wcid = 0;

                            if (values.Count >= 2 && Int32.TryParse(values[0], out wcid)) {
                                if (!Weenies.ContainsKey(wcid)) {
                                    Weenies.Add(wcid, values[1].Trim());
                                }
                            }
                        }
                    }
                    catch (Exception ex) { Util.LogException(ex); }
                }
            }

            Util.WriteToChat("New weenies list loaded.");
            
            OnWeeniesLoaded(new WeeniesLoadedEventArgs());
        }
    }
}
