﻿namespace WeenieErector.Data.WorldSpawns {
    public class Link {
        public string _linkinfo = "";
        public uint source = 0;
        public uint target = 0;
    }
}