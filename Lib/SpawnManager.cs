﻿using System;
using System.Collections.Generic;
using System.IO;
using Decal.Adapter.Wrappers;
using WeenieErector.Data.WorldSpawns;
using Newtonsoft.Json;

namespace WeenieErector.Lib {
    public class SpawnCreatedEventArgs : EventArgs {
        public Weenie Created { get; set; }
    }

    public class SpawnEditedEventArgs : EventArgs {
        public Weenie Edited { get; set; }
    }

    public class SpawnDeletedEventArgs : EventArgs {
        public Weenie Deleted { get; set; }
    }

    public class SpawnFileImportedEventArgs : EventArgs {
        public String File { get; set; }
    }

    public class SpawnsChangedEventArgs : EventArgs {
    }

    public class WcidSpawnedEventArgs : EventArgs {
        public Weenie Weenie { get; set; }
    }

    public class SpawnManager : IDisposable {
        private const int DESPAWN_TIME_MS = 500;
        public bool HasChanges = false;
        public string loadedWorldSpawnsFile = "None!";

        private WorldSpawns worldSpawns = new WorldSpawns();
        private int latestWcidSpawned = 0;
        private DateTime lastThought = DateTime.UtcNow;
        private DateTime removeThisIdAt = DateTime.MaxValue;
        private int removeThisId = 0;
        private int spawnWcid = 0;
        private Weenie newestWeenie = null;
        private bool disposed = false;

        public List<string> modifiedWeenies = new List<string>();
        public List<string> addedWeenies = new List<string>();

        public event EventHandler<SpawnCreatedEventArgs> SpawnCreated;
        public event EventHandler<SpawnEditedEventArgs> SpawnEdited;
        public event EventHandler<SpawnDeletedEventArgs> SpawnDeleted;
        public event EventHandler<SpawnFileImportedEventArgs> SpawnFileImported;
        public event EventHandler<SpawnsChangedEventArgs> SpawnsChanged;
        public event EventHandler<WcidSpawnedEventArgs> WcidSpawned;

        public SpawnManager() {
            Globals.Core.WorldFilter.ChangeObject += WorldFilter_ChangeObject;
            Globals.Core.WorldFilter.ReleaseObject += WorldFilter_ReleaseObject;
            Globals.Core.WorldFilter.CreateObject += WorldFilter_CreateObject;
        }

        private void WorldFilter_CreateObject(object sender, CreateObjectEventArgs e) {
            try {
                if (e.New.Type == spawnWcid) {
                    SpawnCreatedEventArgs args = new SpawnCreatedEventArgs();
                    args.Created = null;
                    OnSpawnCreated(args);
                }
            }
            catch (Exception ex) { Util.LogException(ex); }
        }

        private void WorldFilter_ReleaseObject(object sender, ReleaseObjectEventArgs e) {
            try {
                if (removeThisId != 0 && e.Released.Id == removeThisId) {
                    removeThisIdAt = DateTime.UtcNow + TimeSpan.FromMilliseconds(DESPAWN_TIME_MS);
                }
            }
            catch (Exception ex) { Util.LogException(ex); }
        }

        private void WorldFilter_ChangeObject(object sender, ChangeObjectEventArgs e) {
            try {
                if (e.Change == WorldChangeType.IdentReceived && removeThisId != 0 && e.Changed.Id == removeThisId) {
                    DecalProxy.DispatchChatToBoxWithPluginIntercept("/removethis");
                }
            }
            catch (Exception ex) { Util.LogException(ex); }
        }

        protected virtual void OnSpawnCreated(SpawnCreatedEventArgs e) {
            SpawnCreated?.Invoke(this, e);
        }

        protected virtual void OnSpawnEdited(SpawnEditedEventArgs e) {
            SpawnEdited?.Invoke(this, e);
        }

        protected virtual void OnSpawnDeleted(SpawnDeletedEventArgs e) {
            SpawnDeleted?.Invoke(this, e);
        }

        protected virtual void OnSpawnFileImported(SpawnFileImportedEventArgs e) {
            SpawnFileImported?.Invoke(this, e);
        }

        protected virtual void OnSpawnsChanged(SpawnsChangedEventArgs e) {
            SpawnsChanged?.Invoke(this, e);
        }

        protected virtual void OnWcidSpawned(WcidSpawnedEventArgs e) {
            WcidSpawned?.Invoke(this, e);
        }

        public void Clear() {
            HasChanges = false;
        }

        public void Import(string file) {
            try {
                Util.WriteToChat("Importing: " + file);

                var filePath = Path.Combine(Util.GetWorldSpawnsPath(), file);

                if (!File.Exists(filePath)) {
                    Util.WriteToChat("Couldn't find worldspawns file: " + filePath);
                    return;
                }

                var json = File.ReadAllText(filePath);

                try {
                    worldSpawns = JsonConvert.DeserializeObject<WorldSpawns>(json);

                    loadedWorldSpawnsFile = file;

                    Util.WriteToChat(string.Format("Loaded {0} weenies from {1}", worldSpawns.GetTotalMappedWeenies(), file));

                    SpawnFileImportedEventArgs args = new SpawnFileImportedEventArgs();
                    args.File = file;
                    OnSpawnFileImported(args);
                    OnSpawnsChanged(new SpawnsChangedEventArgs());
                }
                catch (Exception ex) {
                    Util.LogException(ex);

                    Util.WriteToChat("Unable to parse worldspawns file: " + filePath);
                }
            }
            catch (Exception ex) { Util.LogException(ex); }
        }

        public void Export(string file) {
            worldSpawns.SetVersion();
            var filePath = Path.Combine(Util.GetWorldSpawnsPath(), file);
            string json = JsonConvert.SerializeObject(worldSpawns, Formatting.Indented);
            File.WriteAllText(filePath, json);

            Util.WriteToChat(string.Format("Exported {0} weenie definitions to {1}", worldSpawns.GetTotalMappedWeenies(), filePath));
        }

        public List<Weenie> GetWeeniesFromLandblock(uint landblockId) {
            var weenies = new List<Weenie>();

            if (worldSpawns == null) return weenies;

            var landblock = worldSpawns.landblocks.Find(x => (x.key == landblockId || x.key == (int)landblockId));

            if (landblock == null) return weenies;

            return landblock.value.weenies;
        }

        public void Spawn(int wcid, int parentId) {
            if (!Globals.WeenieSource.Weenies.ContainsKey(wcid)) {
                Util.WriteToChat("Invalid wcid to spawn: " + wcid);
                return;
            }

            Util.WriteToChat(string.Format("Spawning: {0} ({1})", wcid, Globals.WeenieSource.Weenies[wcid]));

            latestWcidSpawned = wcid;

            DecalProxy.DispatchChatToBoxWithPluginIntercept("/loc");

            var characterWo = Globals.Core.WorldFilter[Globals.Core.CharacterFilter.Id];
            var landblock = Util.CurrentLandblock();
            var landcell = Util.CurrentCell();
            var worldSpawnWeenie = new Weenie();

            worldSpawnWeenie.wcid = wcid;
            worldSpawnWeenie.desc = Globals.WeenieSource.Weenies[wcid];

            // we are using the players current position for wcid spawn point
            worldSpawnWeenie.pos.objcell_id = landblock + landcell;
            worldSpawnWeenie.pos.frame.angles.SetFromOrientation(characterWo.Orientation());
            worldSpawnWeenie.pos.frame.origin.x = Globals.Core.Actions.LocationX;
            worldSpawnWeenie.pos.frame.origin.y = Globals.Core.Actions.LocationY;
            worldSpawnWeenie.pos.frame.origin.z = Globals.Core.Actions.LocationZ;

            AddWeenieToWorldSpawns(landblock, worldSpawnWeenie, parentId);

            newestWeenie = worldSpawnWeenie;

            SpawnWcid(wcid);
        }

        private void SpawnWcid(int wcid) {
            // noclip
            DecalProxy.DispatchChatToBoxWithPluginIntercept("/vismode 4"); //4211732
            DecalProxy.DispatchChatToBoxWithPluginIntercept("/spawnwcid " + wcid);

            var args = new WcidSpawnedEventArgs();
            args.Weenie = newestWeenie;
            OnWcidSpawned(args);
        }

        private void AddWeenieToWorldSpawns(uint landblockId, Weenie worldSpawnWeenie, int parentId) {
            var landblock = worldSpawns.landblocks.Find(x => (x.key == landblockId || x.key == (int)landblockId));

            if (landblock == null) {
                landblock = new Landblock();
                landblock.key = landblockId;
                worldSpawns.landblocks.Add(landblock);
                AddWeenieToWorldSpawns(landblockId, worldSpawnWeenie, parentId);
                return;
            }

            worldSpawnWeenie.id = (uint)landblock.value.weenies.Count + 1;

            landblock.value.weenies.Add(worldSpawnWeenie);

            if (landblock != null) {
                if (parentId != 0) {
                    var link = new Link();
                    var parent = Globals.SpawnManager.GetWeenieFromLandblock(Util.CurrentLandblock(), (uint)parentId);
                    link.source = (uint)worldSpawnWeenie.id;
                    link.target = (uint)parentId;
                    link._linkinfo = string.Format("{0} ({1}) -> {2} (3)", worldSpawnWeenie.desc, worldSpawnWeenie.wcid, parent.desc, parent.wcid);
                    landblock.value.links.Add(link);
                }
            }

            HasChanges = true;
            addedWeenies.Add(worldSpawnWeenie.GetUniqueKey());

            SpawnCreatedEventArgs args = new SpawnCreatedEventArgs();
            args.Created = worldSpawnWeenie;
            OnSpawnCreated(args);

            OnSpawnsChanged(new SpawnsChangedEventArgs());
        }

        internal void NudgeWeenie(Weenie weenie, double x, double y, double z) {
            weenie.pos.frame.origin.x += x;
            weenie.pos.frame.origin.y += y;
            weenie.pos.frame.origin.z += z;

            modifiedWeenies.Add(weenie.GetUniqueKey());

            newestWeenie = weenie;

             SpawnEditedEventArgs args = new SpawnEditedEventArgs();
            args.Edited = weenie;
            OnSpawnEdited(args);
            OnSpawnsChanged(new SpawnsChangedEventArgs());
        }

        internal void EditWeenie(Weenie weenie, int wcid, string desc, WeeniePos pos, int parentId) {
            double threshold = 0.1;

            uint landblockId = Util.CurrentLandblock();
            var landblock = worldSpawns.landblocks.Find(x => (x.key == landblockId || x.key == (int)landblockId));

            if (landblock != null) {
                var delete = -1;
                for (var i = 0; i < landblock.value.links.Count; i++) {
                    if (landblock.value.links[i].source == weenie.id) {
                        delete = i;
                        break;
                    }
                }

                if (delete != -1) {
                    landblock.value.links.RemoveAt(delete);
                }

                if (parentId != 0) {
                    var link = new Link();
                    var parent = Globals.SpawnManager.GetWeenieFromLandblock(Util.CurrentLandblock(), (uint)parentId);
                    link.source = (uint)weenie.id;
                    link.target = (uint)parentId;
                    link._linkinfo = string.Format("{0} ({1}) -> {2} (3)", weenie.desc, weenie.wcid, parent.desc, parent.wcid);
                    landblock.value.links.Add(link);
                }
            }

            var charX = Globals.Core.Actions.LocationX;
            var charY = Globals.Core.Actions.LocationY;
            var charZ = Globals.Core.Actions.LocationZ;
            var origin = pos.frame.origin;

            var id = FindWeenieWorldObjectId(weenie);

            if (id == 0) {
                if (Math.Abs(charX - origin.x) < threshold && Math.Abs(charY - origin.y) < threshold && Math.Abs(charZ - origin.z) < threshold) {
                    SpawnWcid(weenie.wcid);
                }
            }
            else {
                if (Math.Abs(charX - origin.x) < threshold && Math.Abs(charY - origin.y) < threshold && Math.Abs(charZ - origin.z) < threshold) {
                    RemoveWeenieSpawnedObject(weenie);
                    spawnWcid = wcid;
                }
                else {
                    Util.WriteToChat("Not attempting to move wcid spawn because its location differs from where you are standing.");
                }
            }

            weenie.desc = desc;
            weenie.wcid = wcid;
            weenie.pos = pos;

            modifiedWeenies.Add(weenie.GetUniqueKey());

            newestWeenie = weenie;

            SpawnEditedEventArgs args = new SpawnEditedEventArgs();
            args.Edited = weenie;
            OnSpawnEdited(args);
            OnSpawnsChanged(new SpawnsChangedEventArgs());
        }

        internal void DeleteWeenie(Weenie weenie) {
            uint landblockId = Util.CurrentLandblock();
            var landblock = worldSpawns.landblocks.Find(x => (x.key == landblockId || x.key == (int)landblockId));

            landblock.value.weenies.Remove(weenie);

            var linksToRemove = new List<Link>();

            foreach (var link in landblock.value.links) {
                if (link.target == weenie.id || link.source == weenie.id) {
                    linksToRemove.Add(link);
                }
            }

            foreach (var link in linksToRemove) {
                landblock.value.links.Remove(link);
            }

            landblock.ReindexWeenies();

            RemoveWeenieSpawnedObject(weenie);

            SpawnDeletedEventArgs args = new SpawnDeletedEventArgs();
            args.Deleted = weenie;
            OnSpawnDeleted(args);
            OnSpawnsChanged(new SpawnsChangedEventArgs());
        }

        private bool RemoveWeenieSpawnedObject(Weenie weenie) {
            var threshold = 0.1;

            foreach (var wo in Globals.Core.WorldFilter.GetLandscape()) {
                if (wo.Type != weenie.wcid) continue;
                if (Math.Abs(wo.Offset().X - weenie.pos.frame.origin.x) > threshold) continue;
                if (Math.Abs(wo.Offset().Y - weenie.pos.frame.origin.y) > threshold) continue;
                if (Math.Abs(wo.Offset().Z - weenie.pos.frame.origin.z) > threshold) continue;

                Globals.Core.Actions.SelectItem(wo.Id);
                Globals.Core.Actions.RequestId(wo.Id);
                removeThisId = wo.Id;
                return true;
            }

            return false;
        }

        public int FindWeenieWorldObjectId(Weenie weenie) {
            var threshold = 0.1;

            foreach (var wo in Globals.Core.WorldFilter.GetLandscape()) {
                if (wo.Type != weenie.wcid) continue;
                if (Math.Abs(wo.Offset().X - weenie.pos.frame.origin.x) > threshold) continue;
                if (Math.Abs(wo.Offset().Y - weenie.pos.frame.origin.y) > threshold) continue;
                if (Math.Abs(wo.Offset().Z - weenie.pos.frame.origin.z) > threshold) continue;

                return wo.Id;
            }

            return 0;
        }

        public Weenie GetWeenieParent(Weenie weenie) {
            uint landblockId = Util.CurrentLandblock();
            var landblock = worldSpawns.landblocks.Find(x => (x.key == landblockId || x.key == (int)landblockId));

            if (landblock == null) return null;

            foreach (var link in landblock.value.links) {
                if (link.source == weenie.id) return GetWeenieFromLandblock(landblockId, link.target);
            }

            return null;
        }

        private Weenie GetWeenieFromLandblock(uint landblockId, uint target) {
            var landblock = worldSpawns.landblocks.Find(x => (x.key == landblockId || x.key == (int)landblockId));

            if (landblock == null) return null;

            return landblock.value.weenies.Find(x => (x.id == target));
        }

        public void Think() {
            if (DateTime.UtcNow - lastThought > TimeSpan.FromMilliseconds(100)) {
                lastThought = DateTime.UtcNow;

                if (DateTime.UtcNow > removeThisIdAt) {
                    if (spawnWcid != 0) SpawnWcid(spawnWcid);
                    removeThisIdAt = DateTime.MaxValue;
                    removeThisId = 0;
                }
            }
        }

        public void Dispose() {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing) {
            if (!disposed) {
                if (disposing) {
                    Globals.Core.WorldFilter.ChangeObject -= WorldFilter_ChangeObject;
                }
                disposed = true;
            }
        }
    }
}
