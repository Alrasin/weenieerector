﻿using System;
using System.IO;

namespace WeenieErector.Lib {
    public static class Util {
        public static void LogException(Exception ex) {
            try {
                using (StreamWriter writer = new StreamWriter(Path.Combine(GetPluginStoragePath(), "exceptions.txt"), true)) {
                    writer.WriteLine("============================================================================");
                    writer.WriteLine(DateTime.Now.ToString());
                    writer.WriteLine("Error: " + ex.Message);
                    writer.WriteLine("Source: " + ex.Source);
                    writer.WriteLine("Stack: " + ex.StackTrace);
                    if (ex.InnerException != null) {
                        writer.WriteLine("Inner: " + ex.InnerException.Message);
                        writer.WriteLine("Inner Stack: " + ex.InnerException.StackTrace);
                    }
                    writer.WriteLine("============================================================================");
                    writer.WriteLine("");
                    writer.Close();
                }
            }
            catch {
            }
        }

        public static void WriteToChat(string message) {
            try {
                Globals.Host.Actions.AddChatText("[" + Globals.PluginName + "]: " + message, 5);
            }
            catch (Exception ex) { LogException(ex); }
        }

        public static string GetPluginStoragePath() {
            return Path.Combine(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Personal), "Decal Plugins"), Globals.PluginName);
        }

        public static string GetWorldSpawnsPath() {
            return Path.Combine(GetPluginStoragePath(), "worldspawns");
        }

        internal static string FormatCoordinates(double x, double y) {
            var result = "";

            x = Math.Round(x, 4);
            y = Math.Round(y, 4);

            if (y > 0) {
                result += string.Format("{0}N", y);
            }
            else {
                result += string.Format("{0}S", Math.Abs(y));
            }

            result += ", ";

            if (x > 0) {
                result += string.Format("{0}E", x);
            }
            else {
                result += string.Format("{0}W", Math.Abs(x));
            }

            return result;
        }

        internal static void WriteToChat(object completedQueststr) {
            throw new NotImplementedException();
        }

        internal static uint CurrentLandblock() {
            return (uint)(Globals.Core.Actions.Landcell >> 16 << 16);
        }

        internal static uint CurrentCell() {
            return (uint)(Globals.Core.Actions.Landcell << 16 >> 16);
        }
    }
}